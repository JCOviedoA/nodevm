-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost
-- Tiempo de generación: 06-07-2021 a las 11:03:32
-- Versión del servidor: 10.3.29-MariaDB-0ubuntu0.20.04.1
-- Versión de PHP: 7.4.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `nodevm_bd`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `articulos`
--

CREATE TABLE `articulos` (
  `id` int(11) NOT NULL,
  `codigo` text DEFAULT NULL,
  `nombre` text DEFAULT NULL,
  `laboratorio` text DEFAULT NULL,
  `invima` text DEFAULT NULL,
  `fecha_1` text DEFAULT NULL,
  `fecha_2` text DEFAULT NULL,
  `vigente` int(11) DEFAULT NULL,
  `numero_1` int(11) DEFAULT NULL,
  `numero_2` int(11) DEFAULT NULL,
  `numero_3` int(11) DEFAULT NULL,
  `numero_4` int(11) DEFAULT NULL,
  `descripcion` text DEFAULT NULL,
  `observacion` text DEFAULT NULL,
  `fecha_4` text DEFAULT NULL,
  `fecha_5` text DEFAULT NULL,
  `fecha_si_noi` text DEFAULT NULL,
  `codigo_2` text DEFAULT NULL,
  `codigo_3` text DEFAULT NULL,
  `nombre_2` text DEFAULT NULL,
  `tipo` text DEFAULT NULL,
  `observacion_2` text DEFAULT NULL,
  `observacion_3` text DEFAULT NULL,
  `medicion` text DEFAULT NULL,
  `cantidad` text DEFAULT NULL,
  `especificacion_consumo` text DEFAULT NULL,
  `descripcion_empaque` text DEFAULT NULL,
  `empresa_quimica` text DEFAULT NULL,
  `tipo_empresa` text DEFAULT NULL,
  `actividad_empresa` text DEFAULT NULL,
  `actividad_empresa_2` text DEFAULT NULL,
  `actividad_empresa_3` int(11) DEFAULT NULL,
  `actividad_empresa_4` text DEFAULT NULL,
  `fecha_6` text DEFAULT NULL,
  `fecha_7` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `clientes`
--

CREATE TABLE `clientes` (
  `id` int(11) NOT NULL,
  `nombre` varchar(150) NOT NULL,
  `documento` varchar(20) NOT NULL,
  `correo` varchar(50) NOT NULL,
  `direccion` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `clientes`
--

INSERT INTO `clientes` (`id`, `nombre`, `documento`, `correo`, `direccion`) VALUES
(1, 'PEPITO PEREZ', '1234', 'prueba@gmail.com', 'Sector 1'),
(2, 'cliente2', '123', 'cleinte@gmail.com', 'asefgdfgdfg');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `roles`
--

CREATE TABLE `roles` (
  `id` int(11) NOT NULL,
  `nombre` varchar(150) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `roles`
--

INSERT INTO `roles` (`id`, `nombre`) VALUES
(1, 'Administrador'),
(2, 'Vendedor');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

CREATE TABLE `users` (
  `id` int(6) UNSIGNED NOT NULL,
  `name` varchar(50) NOT NULL,
  `address` varchar(100) NOT NULL,
  `phone` varchar(15) DEFAULT NULL,
  `username` varchar(150) NOT NULL,
  `password` text NOT NULL,
  `id_rol` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `users`
--

INSERT INTO `users` (`id`, `name`, `address`, `phone`, `username`, `password`, `id_rol`) VALUES
(1, 'Administrador', '', '', 'admin', '$2b$10$JG.1zsUC.VPWBdA4pWG84.NIvVSEJOXInLRnqYMGDwRWz5aG/utMK', 1),
(2, 'yilbert', 'sector', '3168456554', 'yilbert', '123456', 2);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `articulos`
--
ALTER TABLE `articulos`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `clientes`
--
ALTER TABLE `clientes`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_rol` (`id_rol`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `articulos`
--
ALTER TABLE `articulos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `clientes`
--
ALTER TABLE `clientes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `users`
--
ALTER TABLE `users`
  MODIFY `id` int(6) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_ibfk_1` FOREIGN KEY (`id_rol`) REFERENCES `roles` (`id`) ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
