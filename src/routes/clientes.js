const router = require('express').Router();

const clientesController = require('../controllers/clientesController');

router.get('/', clientesController.list);
router.post('/add', clientesController.save);
router.get('/edit/:id', clientesController.edit);
router.post('/update/:id', clientesController.update);
router.get('/delete/:id', clientesController.delete);

module.exports = router;
