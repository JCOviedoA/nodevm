const router = require('express').Router();
let upload = require('../config/multer.config.js');

const reportesController = require('../controllers/reportesController');

router.get('/articulos/:page/:per_page', reportesController.listar);
router.post('/import_csv', upload.single("file"), reportesController.uploadFile);
router.get('/file', reportesController.downloadFile);


module.exports = router;
