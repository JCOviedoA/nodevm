const router = require('express').Router();

const usuariosController = require('../controllers/usuariosController');

router.get('/', usuariosController.list);
router.get('/roles', usuariosController.listRoles);
router.post('/add', usuariosController.save);
router.get('/edit/:id', usuariosController.edit);
router.post('/update/:id', usuariosController.update);
router.get('/delete/:id', usuariosController.delete);

module.exports = router;
