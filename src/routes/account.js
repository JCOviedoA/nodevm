const router = require('express').Router();

const loginController = require('../controllers/loginController');

router.get('/auth', loginController.auth);
router.post('/login', loginController.login);
router.post('/add', loginController.save);
router.get('/logout', loginController.logout);

module.exports = router;
