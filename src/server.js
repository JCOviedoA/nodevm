const express = require('express'),
    path = require('path'),
    morgan = require('morgan'),
    bodyParser = require('body-parser'),
    cors = require('cors'),
    mysql = require('mysql'),
    passport = require('passport'),
    cookieParser = require('cookie-parser'),
    session = require('express-session'),
    myConnection = require('express-myconnection'),
    bcrypt = require('bcrypt'),
    env = require('./config/env.js')

    ;

global.__basedir = __dirname;

const app = express();
var port = process.env.PORT || 4000;

// middlewares
app.use(bodyParser.json());
app.use(morgan('dev'));
app.use(myConnection(mysql, env, 'single'));
app.use(cors());
app.use(express.urlencoded({extended: true}));
app.use(cookieParser('secreto'));
app.use(session({
  secret:'secreto',
  resave:true,
  saveUninitialized:true
}));

app.use(passport.initialize());
app.use(passport.session());


// routes

// importing routes

const reportesRoutes = require('./routes/reportes');
app.use('/reportes', reportesRoutes);

const clientesRoutes = require('./routes/clientes');
app.use('/clientes', clientesRoutes);

const usuariosRoutes = require('./routes/usuarios');
app.use('/usuarios', usuariosRoutes);

const accountRoutes = require('./routes/account');
app.use('/account', accountRoutes);

// static file
app.use(express.static(path.join(__dirname, 'public')));

// start the server
var server = app.listen(port, function(){
  console.log('Listening on port ' + port);
});
