require("./bootstrap");
window.Vue = require("vue");
import Vue from 'vue';

import VueAxios from 'vue-axios';
import axios from 'axios';
Vue.use(VueAxios, axios);

/* Insertada por: Gilberto Mendoza
funciones generales
Fecha: 28 de marzo 2019 */
import Fun from "./funciones.js";
Vue.prototype.$Fun = Fun;

/* Insertada por: Gilberto
vee-validate
*/
import VeeValidateEs from "vee-validate/dist/locale/es";
import VeeValidate, {
  Validator
} from "vee-validate";
Vue.use(VeeValidate, {
  inject: true,
  fieldsBagName: "veeFields",
  classes: true,
  classNames: {
    invalid: "is-invalid"
  }
});
Validator.localize("es", VeeValidateEs);

import lodash from "lodash";
window._ = lodash;
Object.defineProperty(Vue.prototype, "$_", {
  value: lodash
});

// Notificaciones tomadas de: https://github.com/sweetalert2/sweetalert2
// Fecha: 15 de Enero 2018
// import swal from 'sweetalert2/dist/sweetalert2.js';
import Swal from "sweetalert2";
Vue.prototype.$swal = Swal;
window.swal = Swal;

/* Insertada por: Gilberto Mendoza
moment
Fecha: 28 de marzo 2019 */
window.moment = require("moment");
import moment from "moment";
//require('moment/locale/es')
moment.updateLocale("es", {
  monthsShort: "Ene_Feb_Mar_Abr_May_Jun_Jul_Ago_Sep_Oct_Nov_Dic".split("_")
});
Vue.prototype.moment = moment;
Object.defineProperty(Vue.prototype, "$moment", {
  value: moment
});

import {
  Form,
  HasError,
  AlertError,
  AlertErrors,
  AlertSuccess
} from "vform";

Vue.component(HasError.name, HasError);
Vue.component(AlertError.name, AlertError);
Vue.component(AlertErrors.name, AlertErrors);
Vue.component(AlertSuccess.name, AlertSuccess);

Vue.use(require('./declareVar').default);

import menuPrincipal from "./components/menu_principal.vue";
Vue.component("menuPrincipal", menuPrincipal);

import ModalComponent from "./components/Modal.vue";
Vue.component("ModalComponent", ModalComponent);

import Paginate from 'vuejs-paginate'
Vue.component('paginate', Paginate)

import App from './App.vue';
import router from "./routes";
new Vue(Vue.util.extend({ router },App)).$mount('#app');
