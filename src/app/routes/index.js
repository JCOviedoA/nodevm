import Vue from 'vue';
import VueRouter from 'vue-router';
Vue.use(VueRouter);

import Auto from './rutas';

/*
| Grupos de rutas
*/

const template = { template: '<router-view/>' };
/*
name: nombre la ruta, es utilizado para navegar de una vista a otra
path: url o ruta, es la que aparece en la url del navegador
component: require(ubicacion de la vista).default
component: template, children: require(ubicacion del archivo de grupo de rutas).default
*/
import App from '../App.vue';

var routes = [
  {
    path: '/',
    name: 'login',
    component: require('../components/account/login.vue').default,
  },
  {
    path: '/home',
    name: 'home',
    component: require('../components/index.vue').default,
  },
  {
    path: '/exportar-csv',
    name: 'exportar_csv',
    component: require('../components/exportar_csv.vue').default,
  },

  { path: '/account', component: template, children: require('./auth').default },

  { path: '/usuarios', component: template, children: require('./usuarios').default },
  { path: '/clientes', component: template, children: require('./clientes').default },


  { path: '404', name: '404', component: require('../components/404.vue').default },

]

const router = new VueRouter({
  mode: "history",
  routes,
  linkExactActiveClass: 'active',
  scrollBehavior(to, from, savedPosition) {
    return { x: 0, y: 0 };
  }
})

router.beforeEach((to, from, next) => {
  next()
})

export default router;
