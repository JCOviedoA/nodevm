// export default function Auto(name, path, component, children =null) {
//     this.name = name;
//     this.path = path;
//     this.component =  component;
//     if(children){
//         this.children =children;
//     }
// }
export default class Auto {
  constructor(name, path, component, children =null) {
    this.name = name;
    this.path = path;
    this.component =  component;
    if(children){
      this.children =children;
    }
  }


}
