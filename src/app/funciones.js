import axios from 'axios';//importar libreria axios
export default class Fun {
  /**
  * Gilberto Mendoza
  * 2019-06-12 16-52
  *
  */
  // funcion de mensajes de confirmacion
  static mensaje(tipo = 1, titulo = '', mensaje = '', icono = 'info', callback1 = false, callback2 = false) {
    switch (tipo) {
      case 1:
      swal(titulo, mensaje, icono);
      break;

      case 2:
      swal({
        title: titulo,
        // text: mensaje,
        html:mensaje,
        type: icono,
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Si',
        cancelButtonText: 'No'
      }).then((result) => {
        if (result.value) {
          if (callback1) {
            callback1()
          }
        } else {
          if (callback2) {
            callback2()
          }
        }
      })
      break;

      case 3:
      swal({
        title: titulo,
        // text: mensaje,
        html:mensaje,
        type: icono,
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Guardar',
        cancelButtonText: 'Cancelar'
      }).then((result) => {
        if (result.value) {
          if (callback1) {
            callback1()
          }
        } else {
          if (callback2) {
            callback2()
          }
        }
      })
      break;

      case 4:
      swal({
        title: titulo,
        // text: mensaje,
        html:mensaje,
        type: icono,
        allowEscapeKey: false,
        onClose: () => {
          if (callback1) {
            callback1()
          }
          return true;
        }
      })
      break;

      case 5:
      swal({
        title: titulo,
        html: mensaje,
        type: icono,
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Aceptar',
        cancelButtonText: 'Cancelar'
      }).then((result) => {
        if (result.value) {
          if (callback1) {
            callback1()
          }
        } else {
          if (callback2) {
            callback2()
          }
        }
      })
      break;
    }
  }
  // funcion efecto cargando
  static cargando(activo = true) {
    if (activo) {
      swal({
        title: 'Procesando...',
        allowOutsideClick: false,
        onOpen: () => {
          swal.showLoading()
        },
      })
    } else {
      swal.close();
    }
  }
  /**
  * Gilberto Mendoza
  * 2018-06-14
  *
  * [deslizar desliza o desplaza por la vista html segun la cordenada de un objeto;
    * tambien podria decirse que realiza un scrolltop hasta el elemento que se pasa como parametro]
    * @param  {[type]}  objeto         [objeto del html de tipo Jquery]
    * @param  {Boolean} [ajuste=false] [sumar o restar a la cordenada]
    * @param  {Boolean} [tiempo=false] [tiempo de  la animacion]
    */
    static deslizar(objeto, ajuste = false, tiempo = false) {
      if (objeto) {
        let limite = objeto.offset().top;
        if (ajuste) {
          limite = limite + ajuste;
        }
        if (tiempo) {
          $("html, body").animate({scrollTop: limite}, tiempo);
        } else {
          $("html, body").animate({scrollTop: limite}, 600);
        }
      } else {
        console.log('el primer argumento o parametro debe ser un objeto del html ');
      }
    }
    // cantidad de decimales que va tener un numero
    static formato_numero(numero, decimales = false) {
      let n;
      n = accounting.formatNumber(numero)
      if (decimales) {
        n = accounting.formatNumber(numero, decimales, '')
      }
      return Fun.intVal(n)
    }
    // validar que el numero digitado sea numerico
    static intVal(i) {
      return typeof i === 'string' ? i.replace(/[\$,]/g, '') * 1 : typeof i === 'number' ? i : 0;
    }
    // | graba el monitoreo en la base de datos
    // | de las acciones que el usuario hace con un click
    static monitorear(evento = false, descripcion = false,callback) {
      try {
        $.ajaxSetup({
          headers: {
            'Authorization': `Bearer ${localStorage.getItem("token")}`
          }
        });
        if (typeof (evento) == 'object') {
          $.ajax({
            url: '/api/monitorear',
            type: 'POST',
            dataType: 'json',
            data: {
              url: location.pathname,
              evento: evento.attr('title'),
              descripcion: evento.attr('description'),
              tipo: evento.attr('type'),
              valor: evento.val(),
              formulario: evento.attr('formulario') != undefined ? $(evento.attr('formulario')).serializeArray() : null,
            },
            success: response => {
              callback(true)
            }
          })
        } else {
          if (typeof (descripcion) == 'object') {
            descripcion = JSON.stringify(descripcion);
          }
          $.ajax({
            url: '/api/monitorear',
            type: 'POST',
            dataType: 'json',
            data: {
              url: location.pathname,
              evento: evento,
              descripcion: descripcion,
            },
            success: response => {
              callback(true)
            }
          })
        }
      } catch (e) {
        console.warn(e.response);
        callback(false)
      } finally {
        callback(true)
      }
    }
    // mostrar mensaje notificacion
    static alerta(title, text, type = 'info', duration = 10000, speed = 1000) {
      vm.$notify({
        // (optional)
        // Name of the notification holder
        group: 'foo',

        // (optional)
        // Class that will be assigned to the notification
        type,

        // (optional)
        // Title (will be wrapped in div.notification-title)
        title,

        // Content (will be wrapped in div.notification-content)
        text,

        // (optional)
        // Overrides default/provided duration
        duration,

        // (optional)
        // Overrides default/provided animation speed
        speed,

        // (optional)
        // Data object that can be used in your template
        data: {}
      })
    }
    // mostrar ventana mensaje de error en las peticiones get y post
    static mensajeError(error) {
      console.log(error);
      swal(error.data.error, `De presentarse continuamente este error comunicarse con el departamento de soporte técnico.${error.data.id?" Ref  000"+error.data.id:""}`, 'warning');
    }
    // array de menses con su nombre y numero
    static meses() {
      let meses = [{
        key: 'Enero',
        value: 1
      },
      {
        key: 'Febrero',
        value: 2
      },
      {
        key: 'Marzo',
        value: 3
      },
      {
        key: 'Abril',
        value: 4
      },
      {
        key: 'Mayo',
        value: 5
      },
      {
        key: 'Junio',
        value: 6
      },
      {
        key: 'Julio',
        value: 7
      },
      {
        key: 'Agosto',
        value: 8
      },
      {
        key: 'Septiembre',
        value: 9
      },
      {
        key: 'Octubre',
        value: 10
      },
      {
        key: 'Noviembre',
        value: 11
      },
      {
        key: 'Diciembre',
        value: 12
      }];
      return meses;
    }
    // puntuaciones numericos
    static formato_miles(numero) {
      return new Intl.NumberFormat("ar-FR").format(numero);
    }
    // listar opciones de un combo
    static setCombo(url,descripcion,callback){
      // utilizar una funcion general axios_get("url","descripcion a la cual lista")(ajax methods get)
      Fun.axios_get(url,e=>{
        var options_combo = e.data?e.data:[]
        options_combo.unshift({id:"",text:"Seleccione una Opción"})
        callback(options_combo)
      })
    }
    // retornar enteros en millones
    static formato_millones(valor) {
      return Fun.formato_numero(valor / 1e6, 1) + ' M';
    }
    /*
    tipos
    0=>usuario
    1=>cliente,
    */
    static getImagen(val, tipo = 0) {
      if (val) {
        var arr = ['/', '/clientes/',]
        return `/api/storage${arr[tipo]}${val}`
      }
      var arr = ['sin_user1.png', 'sin_cliente1.png',]
      return `/img/sin_foto/${arr[tipo]}`
    }
    /**
    * Función para crear un mensaje de error de manera general cuando ingresa al catch despues de un AXIOS,
    * fecha: 02 Abril 2019
    */
    static errorCatch(error, msj) {
      // utilizar una funcion general cargando
      Fun.cargando(0)
      console.error(msj, error)
      // mensaje de notificacion error
      Vue.notify({
        group: 'foo',
        type: "error",
        duration: 10000,
        title: "<p class='f-18'>Error</p>",
        text: `<p class='f-16'>${msj}</p>`
      })
    }
    static thenAxios(res, titulo = "Sin titulo",tipo=1) {
      // utilizar una funcion general cargando
      Fun.cargando(0)
      let respuesta = false
      if (res.data.alert) {
        // mensaje de notificacion alerta
        Vue.notify({
          group: 'foo',
          type: "warn",
          duration: 10000,
          title: "<p class='f-18'>Advertencia</p>",
          text: `<p class='f-16'>${res.data.alert}</p>`
        });
      } else if (!res.data.error) {
        if(tipo){
          // utilizar una funcion general monstrar mensaje menaje(tipo,titulo,descripcion,icono)
          Fun.mensaje(1, titulo, res.data, 'success');
        }
        respuesta = true;
      } else {
        // utilizar una funcion general mostrar ventana mensaje de error en las peticiones get y post
        Fun.mensajeError(res)
      }
      return respuesta
    }
    // consultar en la base de datos si existe el registro en la tabla
    static valorExiste(tabla,campo,valor,callback){
      // utilizar una funcion general axios_get("url","descripcion a la cual lista")(ajax methods get)
      Fun.axios_get(`/api/valor_existe/${tabla}/${campo}/${valor}`,e=>{
        callback({
          success:e.data.success,
          data:e.data.data
        })
      })
    }
    // consultar en la base de datos validar si existe el registro en la tabla
    static validarExiste(tabla,campo,val,id,callback){
      // utilizar una funcion general axios_get("url","descripcion a la cual lista")(ajax methods get)
      Fun.axios_get(`/api/encontrar_existente/${tabla}/${campo}/${id}/${val}`,e=>{
        callback({
          success:e.data.success,
          data:e.data.data
        })
      })
    }
    // consultar metoodo get con parametros y mensaje de confirmacion
    static form_get(url,form,callback){
      // funcion confirmacion
      const confirmConsulta=()=>{
        // utilizar una funcion general cargando
        Fun.cargando()
        // utilizar una funcion general monitero y llevar registro de las peticiones al servidor
        // Fun.monitorear(`Listar ${title}`,form,e=>{})
        // ajax metodo post
        form.post(url).then(res=>{
          // utilizar una funcion general ocultar gif cargando
          Fun.cargando(0)
          if(res.data.alert){
            // mensaje de notificacion alerta
            Vue.notify({
              group: 'foo',
              type: "warn",
              duration: 10000,
              title: "<p class='f-18'>Advertencia</p>",
              text: `<p class='f-16'>${res.data.alert}</p>`
            })
            callback({success:false,data:[]})
          }else if(!res.data.error){
            callback({success:true,data:res.data})
          }else{
            // utilizar una funcion general mostrar ventana mensaje de error en las peticiones get y post
            Fun.mensajeError(res)
            callback({success:false,data:[]})
          }
        }).catch(error=>{
          console.log(error);
          // utilizar una funcion general ocultar gif cargando
          Fun.cargando(0)
          // mensaje de notificacion error
          Vue.notify({
            group: 'foo',
            type: "error",
            duration: 10000,
            title: "<p class='f-18'>Error</p>",
            text: "<p class='f-16'>Sucedio un error al listar</p>"
          })
          callback({success:false,data:[]})
        })
      }
      // utilizar una funcion general monstrar mensaje menaje(tipo,titulo,descripcion,icono,funcion confirmacion)
      Fun.mensaje(5,'','¿Está seguro de consultar la información...?','question',confirmConsulta);
    }
    // consultar metoodo get con parametros
    static _get(url, form, callback){
      // utilizar una funcion general cargando
      Fun.cargando()
      // utilizar una funcion general monitero y llevar registro de las peticiones al servidor
      // Fun.monitorear(`Listar ${title}`,form,e=>{})
      // ajax metodo post
      form.post(url).then(res=>{
        // utilizar una funcion general ocultar gif cargando
        Fun.cargando(0)
        if(res.data.alert){
          // mensaje de notificacion alerta
          Vue.notify({
            group: 'foo',
            type: "warn",
            duration: 10000,
            title: "<p class='f-18'>Advertencia</p>",
            text: `<p class='f-16'>${res.data.alert}</p>`
          })
          callback({success:false,data:[]})
        }else if(!res.data.error){
          callback({success:true,data:res.data})
        }else{
          // utilizar una funcion general mostrar ventana mensaje de error en las peticiones get y post
          Fun.mensajeError(res)
          callback({success:false,data:[]})
        }
      }).catch(error=>{
        Fun.cargando(0)
        console.log(error)
        // mensaje de notificacion error
        Vue.notify({
          group: 'foo',
          type: "error",
          duration: 10000,
          title: "<p class='f-18'>Error</p>",
          text: "<p class='f-16'>Sucedio un error al listar</p>"
        })
        callback({success:false,data:[]})
      })
    }
    // guardar formulario metodo post con archivos adjuntos
    static form_adjunto_save(url,formData,result,errors,callback){
      // funcion confirmacion
      const confirmSave=()=>{
        // utilizar una funcion general cargando
        Fun.cargando()
        // utilizar una funcion general monitero y llevar registro de las peticiones al servidor
        // Fun.monitorear(`Guardar ${title}`,formData,e=>{ })
        // configuracion para aceptar archivos adjuntos
        const config = { headers: { 'Content-Type': 'multipart/form-data' }}
        // ajax metodo post
        axios.post(url,formData,config).then(res=>{
          // utilizar una funcion general ocultar gif cargando
          Fun.cargando(0)
          if(res.data.alert){
            // mensaje de notificacion alerta
            Fun.mensaje(1,"Advertencia",`<p class='f-16'>${res.data.alert}</p>`,"warning")
            callback({success:false,data:res.data})
          }else if(res.data.error){
            // utilizar una funcion general mostrar ventana mensaje de error en las peticiones get y post
            Fun.mensajeError(res)
            callback({success:false,data:res.data})
          }else{
              // mensaje de notificacion exito
              const confirmSuccess=()=>{
                callback({success:true,data:res.data})
              }
              // mensaje de notificacion exito
              Fun.mensaje(4,'Éxito',"Guardado correctamente",'success',confirmSuccess);

            // Fun.mensaje(1,"Éxito","Guardado correctamente","success")
            // callback({success:res,data:res.data})
          }
          // utilizar una funcion general delizar scroll hasta encabezado
          Fun.deslizar($(`#id_content_page`),-200)
        })
        .catch(error=>{
          console.log(error.response)
          // utilizar una funcion general ocultar gif cargando
          Fun.cargando(0)
          // mensaje de notificacion error
          Vue.notify({
            group: 'foo',
            type: "error",
            duration: 10000,
            title: "<p class='f-18'>Error</p>",
            text: "<p class='f-16'>Sucedio un error al guardar</p>"
          })
          callback({success:false,data:[]})
        })
      }
      if (result) {
        // utilizar una funcion general monstrar mensaje menaje(tipo,titulo,descripcion,icono,funcion confirmacion)
        Fun.mensaje(3,'','¿Desea Salvar la Información?','question',confirmSave);
      }else {
        // utilizar una funcion general delizar scroll hasta donde se encuentra el error
        Fun.deslizar($(`[name=${_.head(errors.items).field}]`),-150)
      }
    }
    // guardar formulario metodo post
    static form_save2(url,form,mensaje=null,result,errors,callback){

      // funcion confirmacion
      const confirmSave=()=>{
        // utilizar una funcion general cargando
        Fun.cargando()
        // ajax metodo post
        form.post(url).then(res=>{
          // utilizar una funcion general ocultar gif cargando
          Fun.cargando(0)
          if(res.data.alert){
            // mensaje de notificacion alerta
            Fun.mensaje(1,"Advertencia",`<p class='f-16'>${res.data.alert}</p>`,"warning")
            callback(false)
          }else if(res.data.error){
            // utilizar una funcion general mostrar ventana mensaje de error en las peticiones get y post
            Fun.mensajeError(res)
            callback(false)
          }else{
            const confirmSuccess=()=>{
              callback(res)
            }
            // mensaje de notificacion exito
            mensaje = mensaje?mensaje:"Guardado correctamente"
            Fun.mensaje(4,'Éxito',mensaje,'success',confirmSuccess);
          }
        }).catch(error=>{
          console.log(error.response);
          let msj = ""
          if (error.response) {
            let data = error.response.data
            if (data) {
              if (data.errors) {
                _.forEach(data.errors,value=>{
                  for(let v of value){
                    msj += `<p class='f-12'>*${v}</p>`
                  }
                })
              }
            }
          }
          // utilizar una funcion general ocultar gif cargando
          Fun.cargando(0)
          // mensaje de notificacion error
          Fun.mensaje(1,"Error",`<p class='f-16'>Sucedio un error al guardar</p> ${msj}`,"error")
          callback(false)
        })
      }
      if (result) {
        // utilizar una funcion general monstrar mensaje menaje(tipo,titulo,descripcion,icono,funcion confirmacion)
        Fun.mensaje(3,'','¿Desea Salvar la Información?','question',confirmSave);
      }else {
        // utilizar una funcion general delizar scroll hasta donde se encuentra el error
        Fun.deslizar($(`[name=${_.head(errors.items).field}]`),-150)
      }
    }
    // guardar formulario metodo post
    // static form_save(url,form,title,result,errors,callback){
    static form_save(url,form,result,errors,callback){
      // funcion confirmacion
      const confirmSave=()=>{
        // utilizar una funcion general cargando
        Fun.cargando()
        // utilizar una funcion general monitero y llevar registro de las peticiones al servidor
        // Fun.monitorear(`Guardar ${title}`,form,e=>{ })
        // ajax metodo post
        form.post(url).then(res=>{
          // utilizar una funcion general ocultar gif cargando
          Fun.cargando(0)
          if(res.data.alert){
            // mensaje de notificacion alerta
            Fun.mensaje(1,"Advertencia",`<p class='f-16'>${res.data.alert}</p>`,"warning")
            callback(false)
          }else if(res.data.error){
            // utilizar una funcion general mostrar ventana mensaje de error en las peticiones get y post
            Fun.mensajeError(res)
            callback(false)
          }else{
            // mensaje de notificacion exito
            const confirmSuccess=()=>{
              callback(res.data)
            }
            // mensaje de notificacion exito
            Fun.mensaje(4,'Éxito',"Guardado correctamente",'success',confirmSuccess);
            // Fun.mensaje(1,"Éxito","Guardado correctamente","success")
            // callback(res)
          }
          // utilizar una funcion general delizar scroll hasta encabezado
          // Fun.deslizar($(`#id_content_page`),-200)
        }).catch(error=>{
          console.log(error,error.response);
          let msj = ""
          if (error.response) {
            let data = error.response.data
            if (data) {
              if (data.errors) {
                _.forEach(data.errors,value=>{
                  for(let v of value){
                    msj += `<p class='f-12'>*${v}</p>`
                  }
                })
              }
            }
          }
          // utilizar una funcion general ocultar gif cargando
          Fun.cargando(0)
          // mensaje de notificacion error
          Fun.mensaje(1,"Error",`<p class='f-16'>Sucedio un error al guardar</p> ${msj}`,"error")
          callback(false)
        })
      }
      if (result) {
        // utilizar una funcion general monstrar mensaje menaje(tipo,titulo,descripcion,icono,funcion confirmacion)
        Fun.mensaje(3,'','¿Desea Salvar la Información?','question',confirmSave);
      }else {
        // utilizar una funcion general delizar scroll hasta donde se encuentra el error
        Fun.deslizar($(`[name=${_.head(errors.items).field}]`),-150)
      }
    }
    //ajax metodo get
    static axios_get(url,callback){
      // utilizar una funcion general cargando
      Fun.cargando()
      // utilizar una funcion general monitero y llevar registro de las peticiones al servidor
      // Fun.monitorear(`Listar ${title}`,{},e=>{})
      // ajax metodo get
      axios(url).then(res=>{
        // utilizar una funcion general ocultar gif cargando
        Fun.cargando(0)
        if(res.data.alert){
          // mensaje de notificacion alerta
          Fun.mensaje(1,"Advertencia",`<p class='f-16'>${res.data.alert}</p>`,"warning")
          callback({success:false,data:[]})
        }else if(!res.data.error){
          callback({success:true,data:res.data})
        }else{
          // utilizar una funcion general mostrar ventana mensaje de error en las peticiones get y post
          Fun.mensajeError(res)
          callback({success:false,data:[]})
        }
      })
      .catch(error=>{
        Fun.cargando(0)
        console.log(error)
        // mensaje de notificacion error
        Fun.mensaje(1,"Error","<p class='f-16'>Sucedio un error al listar</p>","error")
        callback({success:false,data:[]})
      })
    }
    //ajax metodo get al digitar en un campo de texto
    static keydwown_get(url,callback){
      // utilizar una funcion general monitero y llevar registro de las peticiones al servidor
      // Fun.monitorear(`Listar ${title}`,{},e=>{})
      // ajax metodo get
      axios(url).then(res=>{
        if(res.data.alert){
          // mensaje de notificacion alerta
          Fun.mensaje(1,"Advertencia",`<p class='f-16'>${res.data.alert}</p>`,"warning")
          callback({success:false,data:[]})
        }else if(!res.data.error){
          callback({success:true,data:res.data})
        }else{
          Fun.mensajeError(res)
          callback({success:false,data:[]})
        }
      })
      .catch(error=>{
        console.log(error)
        // mensaje de notificacion error
        Fun.mensaje(1,"Error","<p class='f-16'>Sucedio un error al listar</p>","error")
        callback({success:false,data:[]})
      })
    }
    //ajax metodo get al digitar en un campo de texto
    static keydwown_form_get(url,form,callback){
      // utilizar una funcion general monitero y llevar registro de las peticiones al servidor
      // Fun.monitorear(`Listar ${title}`,form,e=>{})
      // ajax metodo get
      axios.post(url,form).then(res=>{
        if(res.data.alert){
          // mensaje de notificacion alerta
          Fun.mensaje(1,"Advertencia",`<p class='f-16'>${res.data.alert}</p>`,"warning")
          callback({success:false,data:[]})
        }else if(!res.data.error){
          callback({success:true,data:res.data})
        }else{
          Fun.mensajeError(res)
          callback({success:false,data:[]})
        }
      })
      .catch(error=>{
        console.log(error)
        // mensaje de notificacion error
        Fun.mensaje(1,"Error","<p class='f-16'>Sucedio un error al listar</p>","error")
        callback({success:false,data:[]})
      })
    }
    // metodo get eliminar un registro de la tabla con mensaje de confirmacion
    // static axios_delete(url, title, id, callback){
    static axios_delete(url, id, callback){
      // funcion confirmacion
      const confirmDelete=()=>{
        // utilizar una funcion general cargando
        Fun.cargando()
        // utilizar una funcion general monitero y llevar registro de las peticiones al servidor
        // Fun.monitorear(`Eliminar ${title}`,{},e=>{})
        // ajax metodo get
        axios(url).then(res=>{
          // utilizar una funcion general ocultar gif cargando
          Fun.cargando(0)
          if(res.data.alert){
            // mensaje de notificacion alerta
            Fun.mensaje(1,"Advertencia",`<p class='f-16'>${res.data.alert}</p>`,"warning")
            callback(false)
          }else if(res.data.error){
            // utilizar una funcion general mostrar ventana mensaje de error en las peticiones get y post
            Fun.mensajeError(res)
            callback(false)
          }else{
            // mensaje de notificacion exito
            const confirmSuccess=()=>{
              callback(res)
            }
            // mensaje de notificacion exito
            Fun.mensaje(4,'Éxito',"Eliminó correctamente",'success',confirmSuccess);
            // Fun.mensaje(1,"Éxito","Guardado correctamente","success")
            // callback(res)
          }
        })
        .catch( error => {
          // utilizar una funcion general ocultar gif cargando
          Fun.cargando(0)
          console.log(error)
          callback(false)
        })
      }
      if (id) {
        // utilizar una funcion general monstrar mensaje menaje(tipo,titulo,descripcion,icono,funcion confirmacion)
        Fun.mensaje(2,'','¿Desea Eliminar la Información?','question',confirmDelete);
      }else {
        // mensaje de notificacion alerta
        Fun.mensaje(1,"Advertencia","<p class='f-16'>Debe seleccionar un registro existente del sistema</p>","warning")
      }
    }
    // digitar solo numero en un campo de texto
    static solo_numeros(valor_campo){
      var num_caracteres = _.toString(valor_campo).length
      var val_int = ""
      var num_puntos = 0
      var num_decimal = 0
      if (num_caracteres > 0) {
        for (var i = 0; i <= num_caracteres; i++) {
          // funcion de lodash _.isInteger(valor) Checks si el valor es entero
          if (_.isInteger(parseInt(valor_campo[i]))) {
            if (num_puntos == 1 && num_decimal == 2) {
              break
            }else if (num_puntos == 1) {
              num_decimal++
            }
            val_int += valor_campo[i]
          }else if (valor_campo[i] == "." && num_puntos == 0) {
            val_int += valor_campo[i]
            num_puntos++
          }else {
            val_int += ""
          }
        }
      }
      return val_int
    }
    // digitar solo numero en un campo de texto maximo valor
    static maxlength(valor_campo,max){
      var val_int = Fun.solo_numeros(valor_campo)
      return parseFloat(val_int)<=max?val_int:""
    }
    // digitar solo numero en un campo de texto maximo valor
    static formato_moneda(valor_campo,signo_mil=",",prefix=""){
      var var_return = ""
      var val_int = _.isString(valor_campo)?Fun.solo_numeros(valor_campo):Fun.solo_numeros(_.toString(valor_campo))
      if (val_int == ".") {
        return ""
      }
      if (val_int) {
        let exist_point = val_int.split(".")
        var num_int = _.toString(parseFloat(exist_point[0]))
        var z = 0
        for (var i = (num_int.length -1); i >= 0; i--) {
          if (z == 3) {
            var_return = `${num_int[i]}${signo_mil}${var_return}`
            z = 0
          }else {
            var_return = `${num_int[i]}${var_return}`
          }
          z++
        }
        return `${prefix}${var_return}`
      }else {
        return ""
      }
    }
    // digitar solo numero en un campo de texto maximo valor
    static formato_moneda_decimales(valor_campo,signo_mil=",",prefix=""){
      var var_return = ""
      var val_int = _.isString(valor_campo)?Fun.solo_numeros(valor_campo):Fun.solo_numeros(_.toString(valor_campo))
      if (val_int) {
        let exist_point = val_int.split(".")
        var num_int = _.toString(parseFloat(exist_point[0]))
        var last_points = exist_point.length > 1?`.${exist_point[1]}`:""
        var z = 0
        for (var i = (num_int.length -1); i >= 0; i--) {
          if (z == 3) {
            var_return = `${num_int[i]}${signo_mil}${var_return}`
            z = 0
          }else {
            var_return = `${num_int[i]}${var_return}`
          }
          z++
        }
        return last_points ? `${prefix}${var_return}${last_points}`:`${prefix}${var_return}`
      }else {
        return ""
      }
    }
    // completar cantidad de caracteres restantes del lado izquierdo
    static padl(valor_campo,total_digitos,chart){
      var string_total = ""
      // Convierte valor en una string
      for (var i = _.toString(valor_campo).length; i < parseInt(total_digitos); i++) {
        string_total += chart
      }
      return string_total+""+valor_campo
    }
    // completar cantidad de caracteres restantes del lado derecho
    static padr(valor_campo,total_digitos,chart){
      var string_total = ""
      // funcion de lodash _.toString(valor) Convierte valor en un string
      for (var i = _.toString(valor_campo).length; i < parseInt(total_digitos); i++) {
        string_total += chart
      }
      return valor_campo+""+string_total
    }
    // parecido a la funcion like de mysql
    static match_array(array,campo,val_filter){
      const regEx=(str)=>{
        // funcion de lodash _.toLower(valor) Convierte en un string a minúsculas
        var regex = new RegExp( _.toLower(str), 'g' )
        return regex
      }
      if (val_filter) {
        // funcion de lodash _.filter(filtrar y retorna arreglo) _.toLower(valor) Convierte en un string a minúsculas
        var data_filter = _.filter(array,v=>_.toLower(v[campo]).match(regEx(val_filter)))
      }else {
        data_filter = array
      }
      return data_filter
    }
    //funcion generar reporte pdf jasperstudio
    static generarPDF(params){
      let name_report = params[0]
      let ext = params[1]
      let params_report = params[2]
      var params_pdf = ""
      var k = 0
      for(let v of params_report){
        params_pdf += k == (params_report.length - 1)?v.key+"{]"+v.value:v.key+"{]"+v.value+"]["
        k++
      }
      var url = `${name_report}}{${ext}}{${params_pdf}`
      // ajax metodo post
      axios.post("/api/ecncryp_url",{url})
      .then(({data})=>{
        // funcion js abrir otra pestaña del navegador y generar reporte
        window.open(`/informes/${name_report}/${data}`, '_blank');
      })
      .catch(error=>{
        console.log(error)
      })
    }
    static generarExcel(params){
      let name_report = params[0]
      let params_report = params[1]
      let fill_report = params[2]
      var params_pdf = ""
      var fill_pdf = ""
      var k = 0
      var i = 0
      for(let v of params_report){
        params_pdf += i == (params_report.length - 1)?`${v.key}{]${v.value}`:`${v.key}{]${v.value}][`
        i++
      }
      for(let v of fill_report){
        fill_pdf += k == (fill_report.length - 1)?`${v}`:`${v}][`
        k++
      }
      var url = `${params_pdf}}{${fill_pdf}`
      // ajax metodo post
      axios.post("/api/ecncryp_url",{url})
      .then(({data})=>{
        // funcion js abrir otra pestaña del navegador y generar reporte
        window.open(`/informes/export_excel/${name_report}/${data}`, '_blank');
      })
      .catch(error=>{
        console.log(error)
      })
    }

    static find_text(options,id){
      let text = "Todos"
      if (id!="Todos") {
        let data_find = _.find(options,["id",parseInt(id)])
        text = data_find?data_find.text:""
      }else if (!id) {
        text = ""
      }
      return text
    }
    // capturar el ancho de la pantalla
    static innerWidth(){
      return window.innerWidth
    }
    // activar seleccion hora en el campo
    static setFlatpickrHoursMin(attri,hora=moment().format("HH:mm"),hora_min=moment().format("HH:mm")){
      var defaultDate = hora?moment(hora,"HH:mm").format("HH:mm"):moment().format("HH:mm")
      var minDate = hora_min?moment(hora_min,"HH:mm").format("HH:mm"):moment().format("HH:mm")
      defaultDate = defaultDate<=minDate?minDate:defaultDate
      let t = flatpickr(attri, {
        enableTime: true,
        noCalendar: true,
        altInput: true,
        dateFormat: "H:i",
        minDate,
        defaultDate,
      })
      return defaultDate
    }
    // activar seleccion hora en el campo
    static setFlatpickrHoursMax(attri,hora=moment().format("HH:mm"),hora_max=moment().format("HH:mm")){
      var defaultDate = hora?moment(hora,"HH:mm").format("HH:mm"):moment().format("HH:mm")
      var maxDate = hora_max?moment(hora_max,"HH:mm").format("HH:mm"):moment().format("HH:mm")
      defaultDate = defaultDate>=maxDate?maxDate:defaultDate
      let t = flatpickr(attri, {
        enableTime: true,
        noCalendar: true,
        altInput: true,
        dateFormat: "H:i",
        maxDate,
        defaultDate,
      })
      return defaultDate
    }
    // activar seleccion hora en el campo
    static setFlatpickrHoras(attri,hora=moment().format("HH:mm")){
      var defaultDate = hora?moment(hora,"HH:mm").format("HH:mm"):moment().format("HH:mm")
      let t = flatpickr(attri, {
        enableTime: true,
        noCalendar: true,
        altInput: true,
        dateFormat: "H:i",
        defaultDate,
      })
      return defaultDate
    }
    // activar calendario seleccion fecha en el campo
    static setFlatpickr(attri,date=moment().format("Y-MM-DD")){
      var defaultDate = date?moment(date).format("Y-MM-DD"):moment().format("Y-MM-DD")
      let f = flatpickr(attri, {
        altInput: true,
        altFormat: "d/m/Y",
        dateFormat: "Y-m-d",
        defaultDate
      })
      return defaultDate
    }
    // activar calendario seleccion fecha en el campo limitando con fecha maxima
    static activeFlatPicker(attri,date=moment().format("Y-MM-DD"),date_max=moment().format("Y-MM-DD")){
      var defaultDate = date?moment(date).format("Y-MM-DD"):moment().format("Y-MM-DD")
      var maxDate = date_max?moment(date_max).format("Y-MM-DD"):moment().format("Y-MM-DD")
      defaultDate = defaultDate >= maxDate?maxDate:defaultDate
      let f = flatpickr(attri, {
        altInput: true,
        altFormat: "d/m/Y",
        dateFormat: "Y-m-d",
        maxDate,
        defaultDate
      })
      return defaultDate
    }
    // activar calendario seleccion fecha en el campo limitando con fecha minima
    static activeFlatPickerMin(attri,date=moment().format("Y-MM-DD"),date_min=moment().format("Y-MM-DD")){
      var defaultDate = date?moment(date).format("Y-MM-DD"):moment().format("Y-MM-DD")
      var minDate = date_min?moment(date_min).format("Y-MM-DD"):moment().format("Y-MM-DD")
      defaultDate = defaultDate <= minDate?minDate:defaultDate
      let f = flatpickr(attri, {
        altInput: true,
        altFormat: "d/m/Y",
        dateFormat: "Y-m-d",
        minDate,
        defaultDate
      })
      return defaultDate
    }
    // activar calendario seleccion fecha en el campo limitando con fecha minima y maxima
    static activeFlatPickerMinMax(attri,date=moment().format("Y-MM-DD"),date_min=moment().format("Y-MM-DD"),date_max=moment().format("Y-MM-DD")){
      var defaultDate = date?moment(date).format("Y-MM-DD"):moment().format("Y-MM-DD")
      var minDate = date_min?moment(date_min).format("Y-MM-DD"):moment().format("Y-MM-DD")
      var maxDate = date_max?moment(date_max).format("Y-MM-DD"):moment().format("Y-MM-DD")
      defaultDate = defaultDate <= minDate?minDate:defaultDate
      let f = flatpickr(attri, {
        altInput: true,
        altFormat: "d/m/Y",
        dateFormat: "Y-m-d",
        maxDate,
        minDate,
        defaultDate
      })
      return defaultDate
    }
    // mensaje de notificacion de alerta
    static showAlert(msj,id){
      new Promise(resolve=>{
        // mensaje de notificacion alerta
        Vue.notify({
          group: 'foo',
          type: "warn",
          duration: 10000,
          title: "<p class='f-18'>Advertencia</p>",
          text: `<p class="f-16">${msj}</p>`
        })
        resolve(true)
      }).then(res=>{
        if (id) {
          // utilizar una funcion general focus
          Fun.activeFocus(id)
        }
      })
    }
    // saber que sitema operativo se ejecuta el servidor
    static searchSO(){
      var OSName="Desconocido";
      if (navigator.appVersion.match("Windows","g")) OSName="Windows";
      else if (navigator.appVersion.match("Linux","g")) OSName="Linux";
      else if (navigator.appVersion.match("X11","g")) OSName="UNIX";
      else if (navigator.appVersion.match("Mac","g")) OSName="MacOS";
      else if (navigator.appVersion.match("Android","g")) OSName="Android";
      // funcion js abrir otra pestaña del navegador y generar reporte
      window.open(`/scanner/${OSName}`, '_blank');
    }
    // obtener registro de la tabla parametros en la base de datos
    static parametros(campo,callback){
      // utilizar una funcion general axios_get("url","descripcion a la cual lista")(ajax methods get)
      Fun.axios_get("/api/select/data_parametros",e=>{
        callback(e.success?e.data[campo]:"")
      })
    }
    // obtener listado de registros de la tabla parametros en la base de datos
    static listar_parametros(callback){
      // utilizar una funcion general axios_get("url","descripcion a la cual lista")(ajax methods get)
      Fun.axios_get("/api/select/data_parametros",e=>{
        callback(e.data)
      })
    }
    // obtener listado de registros de la tabla parametros en la base de datos
    static listarParametrosContabilidad(campo,callback){
      // utilizar una funcion general axios_get("url","descripcion a la cual lista")(ajax methods get)
      Fun.axios_get("/api/select/combo/parametros_contabilidad/nombre",e=>{
        let param_conta = _.find(e.data,["nombre",campo])
        callback(e.success?param_conta.descripcion:"")
      })
    }
    // obtener listado de registros de la tabla parametros en la base de datos
    static listarParametrosPresupuesto(campo,callback){
      // utilizar una funcion general axios_get("url","descripcion a la cual lista")(ajax methods get)
      Fun.axios_get("/api/select/combo/parametros_presupuesto/nombre",e=>{
        let param_conta = _.find(e.data,["nombre",campo])
        callback(e.success?param_conta.descripcion:"")
      })
    }
    // activar focus con tiempo de espera
    static activeFocus(id){
      const active=()=>{
        $(`#${id}`).focus()
      }
      // funcion js para tomarce 1000 milesegundo para realizar la funcion openModal
      setTimeout(active,1000)
    }
    // eliminar carpeta
    static deleteFolder(folder,callback){
      // utilizar una funcion general axios_get("url","descripcion a la cual lista")(ajax methods get)
      Fun.axios_get(`/api/deleteFolder/${folder}`,e=>{
        callback(e.data)
      })
    }
    // eliminar obtener mac del pc
    static getArp(callback){
      let no_found = "No se encontró MAC de su pc"
      // utilizar una funcion general cargando
      Fun.cargando()
      // utilizar una funcion general axios_get("url","descripcion a la cual lista")(ajax methods get)
      Fun.monitorear(`obtener mac pc`,{},e=>{})
      axios.post("/api/get_mac_pc")
      .then(({data})=>{
        let dt = _.toString(data)
        let ether = dt.split(" [ether] ")
        let at = ether[0].split(" at ")
        callback(at.length > 1?at[1].trim():no_found)
        // utilizar una funcion general ocultar gif cargando
        Fun.cargando(0)
      })
      .catch(error=>{
        callback(no_found)
        console.log(error);
        // utilizar una funcion general ocultar gif cargando
        Fun.cargando(0)
      })
    }
    // calcular valor cuota
    static calcularValorCuota(tasa_interes,cuotas,saldo){
      if (cuotas && saldo) {
        let n_cuotas = parseInt(cuotas)
        let n_saldo = parseFloat(_.replace(saldo,/,/g, ''))
        let n_tasa_interes = parseFloat(tasa_interes) / 100
        if (n_tasa_interes) {
          let tasa_interes_1 = 1 + n_tasa_interes
          let n_ = (Math.pow(tasa_interes_1,  n_cuotas)) * n_tasa_interes
          let d_ = (Math.pow(tasa_interes_1,  n_cuotas)) - 1
          return d_ > 0 ?Math.round((n_ / d_) * n_saldo):0
        }else {
          var valor_return = Math.round(n_saldo / n_cuotas)
          return valor_return
        }
      }else {
        return 0
      }
    }
    // calcular numero cuota
    static calcularNumeroCuota(tasa_interes,valor_cuotas,saldo){
      if (valor_cuotas && saldo) {
        let n_tasa_interes = parseFloat(tasa_interes) / 100
        let n_saldo = parseFloat(_.replace(saldo, /,/g, ''))
        let n_valor_cuotas = parseFloat(_.replace(valor_cuotas, /,/g, ''))
        let n_ = n_valor_cuotas - (n_saldo * n_tasa_interes)
        if (n_ <= 0) {
          return 0
        }else {
          if (n_tasa_interes > 0) {
            let a_ = Math.log(n_valor_cuotas / n_)
            let d_ = Math.log(1 + n_tasa_interes)
            return Math.round(a_/d_)
          }else {
            return Math.round(n_saldo / n_valor_cuotas)
          }
        }
      }else {
        return 0
      }
    }
    // listar progfactu
    static listarProgFactu(id_ciclo,id_municipio,callback){
      // utilizar una funcion general setCombo("url","descripcion a la cual lista")(ajax methods get) ubicado resources/js/funciones.js
      Fun.axios_get(`/api/facturacion_cartera/cartera/listar_progfactu/${id_ciclo}/${id_municipio}`,e=>{
        var options_periodos = []
        if (e.data) {
          let date_base = moment(`01-${e.data.mes_sigu}-${e.data.ano_sigu}`,"DD-MM-YY").format("DD-MM-Y")
          for (var i = 0; i < 12; i++) {
            var fecha_ciclo = moment(date_base,"DD-MM-Y").add(i,"M").format("DD-MM-Y")
            options_periodos.push({
              id:moment(fecha_ciclo,"DD-MM-Y").format("YYMM"),
              text:`Año: ${moment(fecha_ciclo,"DD-MM-Y").format("YY")} / Mes: ${moment(fecha_ciclo,"DD-MM-Y").format("MM")}`
            })
          }
          options_periodos.unshift({id:"",text:"Seleccione una Opción"})
        }
        callback(options_periodos)
      })
    }
    // calcular el valor interes a la fecha
    static calcularInteresFecha(_saldo,_tasainte,_dias){
      return Math.round((((_saldo * _tasainte) / 100) * _dias) / 30)
    }

    static access_tokken(tokken_session,callback){

      try {
        axios("/api/tokken_session").then(({data})=>{
          if (data.success) {
            if (data.tokken != tokken_session) {
              location.href = "/logout";
              callback(false)
            }else {
              callback(true)
            }
          }else {
            callback(false)
            location.href = "/logout";
          }
        }).catch(error=>{
          console.log(error);
          callback(false)
          location.href = "/logout";
        })
      } catch (e) {
        console.log(e.response) // undefined
        callback(false)
        location.href = "/logout";
      }

    }
    // primera letra en mayuscula
    static firstCase(str){
      return _.upperFirst(str)
      // primera letra de cada palabra en mayuscula
      // return str.replace(/\b[a-z]/g,c=>c.toUpperCase())
    }

    static toUpper(string){
      return _.toUpper(string)
    }

  }
