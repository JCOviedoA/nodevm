const controller = {};
const bcrypt = require('bcrypt');

/* GET home page. */
controller.auth = (req, res) => {

  if(req.session.flag == 1){
    req.session.destroy();
    res.json({ title: 'CodeLanguage', message : 'Email ya existe' , flag : 1});
    // res.render('index', { title: 'CodeLanguage', message : 'Email Already Exists' , flag : 1});
  }
  else if(req.session.flag == 2){
    req.session.destroy();
    res.json({ title: 'CodeLanguage', message : 'Registro Exitoso. Por favor Inicie Sesión.', flag : 0});
    // res.render('index', { title: 'CodeLanguage', message : 'Registration Done. Please Login.', flag : 0});
  }
  else if(req.session.flag == 3){
    req.session.destroy();
    res.json({ title: 'CodeLanguage', message : 'Confirme Contraseña No Coinciden.', flag : 1});
    // res.render('index', { title: 'CodeLanguage', message : 'Confirm Password Does Not Match.', flag : 1});
  }
  else if(req.session.flag == 4){
    req.session.destroy();
    res.json({ title: 'CodeLanguage', message : 'Incorrecto Email o Contraseña.', flag : 1 });
    // res.render('index', { title: 'CodeLanguage', message : 'Incorrect Email or Password.', flag : 1 });
  }
  else{
    res.json({ title: 'CodeLanguage', info:req.session.info });
    // res.render('index', { title: 'CodeLanguage' });
  }
};

controller.login = (req, res) => {

  const username = req.body.username;
  const password = req.body.password;
  req.getConnection((error, conn) => {

    var sql_ = `SELECT u.*,r.nombre as rol
    FROM users u,roles r
    WHERE r.id = u.id_rol AND u.username = ?`;

    conn.query(sql_, [username], (err, result, fields) => {

      if(err) throw err;

      if(result.length && bcrypt.compareSync(password, result[0].password)){

        req.session.info = {
          id: result[0].id,
          username: result[0].username,
          name: result[0].name,
          address: result[0].address,
          phone: result[0].phone,
          rol: result[0].rol,
        }

        res.json({ title: 'start session', success:true });
        // res.redirect('/home');
      }else{
        req.session.flag = 4;
        res.json({ title: 'log session', success:false });
        // res.redirect('/');
      }

    });
  });
};

controller.save = (req, res) => {

  var name = req.body.name;
  var address = req.body.address;
  var phone = req.body.phone;
  var username = req.body.username;
  var password = req.body.password;
  var cpassword = req.body.cpassword;
  var id_rol = 2;

  if(cpassword == password){

    req.getConnection((error, conn) => {
      var sql = 'select * from users where username = ?;';
      conn.query(sql,[username], (err, result, fields) => {
        if(err) throw err;

        if(result.length > 0){

          req.session.flag = 1;
          res.json({error,success:false});
          // res.redirect('/');

        }else{

          req.getConnection((error, connection) => {
            var hashpassword = bcrypt.hashSync(password, 10);
            var sql = 'insert into users(name,address,phone,username,password,id_rol) values(?,?,?,?,?,?);';
            const query = connection.query(sql, [name,address,phone,username,hashpassword], (err, result, fields) => {
              if(err) throw err;
              req.session.flag = 2;
              res.json({success:true});
              // res.redirect('/');
            })
          })

        }

      });
    });

  }else{
    req.session.flag = 3;
    res.json({success:false});
    // res.redirect('/');
  }

};

controller.logout = (req, res) => {
  if(req.session.info){
    req.session.destroy();
  }
  res.json({ title: 'close session', success:true });
};

module.exports = controller;
