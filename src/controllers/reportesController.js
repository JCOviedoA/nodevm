const controller = {};
const stream = require('stream');
const await = require('await')
const fs = require('fs');
const path = require('path');

const csv = require('fast-csv');
const Json2csvParser = require('json2csv').Parser;

controller.listar = (req, res) => {
  const { page } = req.params;
  const { per_page } = req.params;

  console.log(page,per_page)

  req.getConnection((error, conn) => {
    conn.query('SELECT count(*) as total_rows FROM articulos limit 1', (error, data) => {
      const limit_ = parseInt(per_page)
      const page_ = parseInt(page)
      let total_rows = data.length?data[0].total_rows:0;
      let total_pages = Math.ceil(parseInt(total_rows)/limit_);
      let offset_ = (page_ - 1) * limit_

      conn.query('SELECT * FROM articulos limit ? offset ?',[limit_,offset_], (error, articulos) => {
        res.json({error,articulos,total_rows,total_pages});
      });

    });
  });
};

/**
* Upload Single CSV file/ and Import data to MySQL/PostgreSQL database
* @param {*} req
* @param {*} res
*/

controller.uploadFile = (req, res) => {
  try {

    const data_articulos = [];
    const url = __basedir + "/uploads/" + req.file.filename
    console.log(url)
    fs.createReadStream(url)
    .pipe(csv.parse({ headers: true }))
    .on('error', error => {
      console.error(error);
      throw error.message;
    })
    .on('data', row => {

      data_articulos.push(row);

    }).on('end', () => {

      req.getConnection((error, connection) => {

        var sql_ = "LOAD DATA LOCAL INFILE '"+url+"' INTO TABLE articulos FIELDS TERMINATED BY ',' LINES TERMINATED BY '\n' IGNORE 1 LINES;";

        connection.query(sql_, (error, articulos) => {

          const result = {
            status: "ok",
            filename: req.file.originalname,
            message: "Upload Successfully!",
          }

          res.json(result);

        });

      });

    })

  } catch (e) {
    const result = {
      status: "error",
      filename: e,
      message: "Upload Error!",
    }

    res.json(result);
  }

}

controller.downloadFile = (req, res) => {

  req.getConnection((error, conn) => {
    var sql_ = `SELECT * FROM articulos`;

    conn.query(sql_, (error, articulos) => {
      const jsonCustomers = JSON.parse(JSON.stringify(articulos));
      const csvFields = ['id',
      'codigo',
      'nombre',
      'laboratorio',
      'invima',
      'fecha_1',
      'fecha_2',
      'vigente',
      'numero_1',
      'numero_2',
      'numero_3',
      'numero_4',
      'descripcion',
      'observacion',
      'fecha_4',
      'fecha_5',
      'fecha_si_noi',
      'codigo_2',
      'codigo_3',
      'nombre_2',
      'tipo',
      'observacion_2',
      'observacion_3',
      'medicion',
      'cantidad',
      'especificacion_consumo',
      'descripcion_empaque',
      'empresa_quimica',
      'tipo_empresa',
      'actividad_empresa',
      'actividad_empresa_2',
      'actividad_empresa_3',
      'actividad_empresa_4',
      'fecha_6',
      'fecha_7'];
      const json2csvParser = new Json2csvParser({ csvFields });
      const csvData = json2csvParser.parse(jsonCustomers);

      res.setHeader('Content-disposition', 'attachment; filename=articulos.csv');
      res.set('Content-Type', 'text/csv');
      res.status(200).end(csvData);

     // res.json({error,articulos});

    });
  });

}

module.exports = controller;
