const controller = {};

controller.list = (req, res) => {
  req.getConnection((error, conn) => {
    conn.query('SELECT * FROM clientes', (error, clientes) => {
     res.json({error,clientes});
    });
  });
};

controller.save = (req, res) => {
  const data = req.body;
  req.getConnection((error, conn) => {
    conn.query('INSERT INTO clientes set ?', data, (error, clientes) => {
      res.json({error});
    });
  });
};

controller.edit = (req, res) => {
  const { id } = req.params;
  req.getConnection((error, conn) => {
    conn.query("SELECT * FROM clientes WHERE id = ?", [id], (error, rows) => {
      const clientes = rows.length?rows[0]:{};
      res.json({error,clientes});
    });
  });
};

controller.update = (req, res) => {
  const { id } = req.params;
  const newCustomer = req.body;
  req.getConnection((err, conn) => {

    conn.query('UPDATE clientes set ? where id = ?', [newCustomer, id], (err, rows) => {
      res.json({err});
    });

  });
};

controller.delete = (req, res) => {
  const { id } = req.params;
  req.getConnection((err, conn) => {
    conn.query('DELETE FROM clientes WHERE id = ?', [id], (err, rows) => {
      res.json({err});
    });
  });
}

module.exports = controller;
