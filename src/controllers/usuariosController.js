const controller = {};

controller.list = (req, res) => {
  req.getConnection((error, conn) => {
    conn.query('SELECT users.*,roles.nombre as rol FROM users,roles where users.id_rol = roles.id', (error, users) => {
     res.json({error,users});
    });
  });
};

controller.listRoles = (req, res) => {
  req.getConnection((error, conn) => {
    conn.query('SELECT * FROM roles', (error, roles) => {
     res.json({error,roles});
    });
  });
};

controller.save = (req, res) => {
  const data = req.body;
  console.log(req.body)
  req.getConnection((error, connection) => {
    const query = connection.query('INSERT INTO users set ?', data, (error, users) => {
      res.json({error,users});
    })
  })
};

controller.edit = (req, res) => {
  const { id } = req.params;
  req.getConnection((error, conn) => {
    conn.query("SELECT * FROM users WHERE id = ?", [id], (error, rows) => {
      const users = rows.length?rows[0]:{};
      res.json({error,users});
    });
  });
};

controller.update = (req, res) => {
  const { id } = req.params;
  const newCustomer = req.body;
  req.getConnection((err, conn) => {

    conn.query('UPDATE users set ? where id = ?', [newCustomer, id], (err, rows) => {
      res.json({err});
    });

  });
};

controller.delete = (req, res) => {
  const { id } = req.params;
  req.getConnection((err, connection) => {
    connection.query('DELETE FROM users WHERE id = ?', [id], (err, rows) => {
      res.json({err});
    });
  });
}

module.exports = controller;
