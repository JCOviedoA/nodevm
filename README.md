# README #
crud sencillo de nodejs backend y vuejs froend
- npm install
- sudo mysql -u root -p

- CREATE database crudnodejsmysql;
- use crudnodejsmysql;

CREATE TABLE `customer` (
  `id` int(6) UNSIGNED NOT NULL PRIMARY KEY,
  `name` varchar(50) NOT NULL,
  `address` varchar(100) NOT NULL,
  `phone` varchar(15) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

- npm run dev
- http://localhost:4000/
